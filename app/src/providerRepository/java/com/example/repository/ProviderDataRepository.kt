package com.example.repository

import com.example.twittercodechallange.model.GetDataResponse
import com.example.twittercodechallange.repository.DataRepository
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import okio.IOException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDateTime.now
import java.util.*
import javax.inject.Inject
import io.reactivex.Observable

class ProviderDataRepository @Inject constructor() : DataRepository {

    lateinit var restService: RestService
    init {
        restService = Retrofit.Builder()
            .baseUrl("http://EnterURlHere")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(getHttpClient())
            .build().create(RestService::class.java)

    }
    override fun getDataForDay(date: Date): Observable<GetDataResponse> {
        return restService.getDataForDay(date.toString())
    }

    private fun getHttpClient(): OkHttpClient? {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient()
            .newBuilder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(object : Interceptor {
                @Throws(IOException::class)
                override fun intercept(chain: Interceptor.Chain): Response {
                    val request: Request = chain.request()
                    val newRequest: Request = request.newBuilder()
                        .addHeader("x-rapidapi-host", "host.com")
                        .addHeader("x-rapidapi-key","validkey")
                        .build()
                    return chain.proceed(newRequest)
                }
            })
            .build()
    }
}