package com.example.repository

import com.example.twittercodechallange.model.GetDataResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

interface RestService {
    @GET("/getDataForDay")
    fun getDataForDay(@Query("date") date: String) : Observable<GetDataResponse>
}