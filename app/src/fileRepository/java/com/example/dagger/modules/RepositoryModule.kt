package com.example.dagger.modules

import com.example.repository.FileDataRepository
import com.example.twittercodechallange.repository.DataRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
public class RepositoryModule {
    @Provides
    fun provideRepository(): DataRepository {
        return FileDataRepository()
    }
}