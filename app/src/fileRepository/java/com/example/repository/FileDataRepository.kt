package com.example.repository

import com.example.twittercodechallange.model.Clouds
import com.example.twittercodechallange.model.GetDataResponse
import com.example.twittercodechallange.model.Weather
import com.example.twittercodechallange.model.Wind
import com.example.twittercodechallange.repository.DataRepository
import io.reactivex.Observable
import java.util.*
import javax.inject.Inject

class FileDataRepository @Inject constructor() : DataRepository {
    var todayData: GetDataResponse = GetDataResponse(
        weather = Weather(temp = 23.0, humidity = 12, pressure = 12),
        wind = Wind(deg = 60, speed = 12.0),
        clouds = Clouds(60)
    )

    override fun getDataForDay(date: Date): Observable<GetDataResponse> {
        return Observable.just(todayData)
    }
}