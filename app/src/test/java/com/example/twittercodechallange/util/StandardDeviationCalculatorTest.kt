package com.example.twittercodechallange.util

import org.junit.Assert
import org.junit.Test

/**
 * In this file we are testing our standard deviation calculator. I only tested std top level function, but we could extend the coverage.
 */
internal class StandardDeviationCalculatorTest {

    @Test
    fun calculateStandardDeviation() {
        val list = listOf<Double>(10.0, 12.0, 23.0, 23.0, 16.0, 23.0, 21.0, 16.0);
        val stdDev = StandardDeviationCalculator.calculate(list)

        Assert.assertEquals(4.8989794855664, stdDev, 0.000001)
    }
}