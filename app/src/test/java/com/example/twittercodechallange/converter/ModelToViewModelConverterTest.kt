package com.example.twittercodechallange.converter

import com.example.twittercodechallange.model.Clouds
import com.example.twittercodechallange.model.GetDataResponse
import com.example.twittercodechallange.model.Weather
import com.example.twittercodechallange.model.Wind
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert
import org.junit.Test

internal class ModelToViewModelConverterTest {

    @Test
    fun convertCurrentConditions() {

    }

    @Test
    fun cloudyIconShown() {
        val responseData = mockk<GetDataResponse>()

        every{ responseData.wind } returns Wind(30, 20.0)
        every{ responseData.weather } returns Weather(23.0, 10, 5)
        every{ responseData.clouds } returns Clouds(20)

        Assert.assertEquals(false, ModelToViewModelConverter.convertCurrentConditions(responseData).cloudy)

        every{ responseData.clouds } returns Clouds(49)
        Assert.assertEquals(false, ModelToViewModelConverter.convertCurrentConditions(responseData).cloudy)

        every{ responseData.clouds } returns Clouds(50)
        Assert.assertEquals(false, ModelToViewModelConverter.convertCurrentConditions(responseData).cloudy)

        every{ responseData.clouds } returns Clouds(51)
        Assert.assertEquals(true, ModelToViewModelConverter.convertCurrentConditions(responseData).cloudy)

        every{ responseData.clouds } returns Clouds(60)
        Assert.assertEquals(true, ModelToViewModelConverter.convertCurrentConditions(responseData).cloudy)

        every{ responseData.clouds } returns Clouds(100)
        Assert.assertEquals(true, ModelToViewModelConverter.convertCurrentConditions(responseData).cloudy)

        every{ responseData.clouds } returns Clouds(101)
        val viewModel = ModelToViewModelConverter.convertCurrentConditions(responseData)
        Assert.assertEquals(ModelToViewModelConverter.ErrorCode.ErrorInvalidCloudPercentage, viewModel.conversionErrorCode)

    }
}