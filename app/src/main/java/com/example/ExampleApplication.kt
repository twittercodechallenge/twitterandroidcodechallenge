package com.example

import android.app.Application
import com.example.twittercodechallange.dagger.components.ApplicationComponent
import com.example.twittercodechallange.dagger.components.DaggerApplicationComponent
//import com.example.twittercodechallange.dagger.components.DaggerApplicationComponent
import com.example.twittercodechallange.dagger.modules.AppModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject


class ExampleApplication : Application(), HasAndroidInjector {
    //private val AF_DEV_KEY = "rqX2Ab6yULzfMSmhjCjsyJ"
    //private val SEGMENT_WRITE_KEY = "bWJAsxKkXar8qvvaNf3T5Cx3TMZbjOhq" //  mK9YTXT87OpbxCDsEy84g1QceW7Fs3Gm
//    private val GOOGLE_PLACES_KEY = "AIzaSyC8Lzh501SdkrdK1CU0ksoYILXWOdlTk2U"
    lateinit var applicationComponent: ApplicationComponent
    companion object {
        private lateinit var instance: ExampleApplication

        @JvmStatic
        fun getInstance() = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        initDagger(this)

    }

    private fun initDagger(app: Application) {

//        val applicationGraph = DaggerApplicationComponent.create()
//        val rep = applicationGraph.repository()
        //val appModule = ApplicationModule(this)
        //providersModule = ProvidersModule(this)
//        applicationComponent = DaggerApplicationComponent.builder()
//            .appModule(AppModule(app))
//            //.applicationModule(appModule)
//            //.transformerModule(TransformerModule(this))
//            //.repositoryModule(RepositoryModule(this))
//            //.providersModule(providersModule)
//
//            .build()

        DaggerApplicationComponent.create()
            .injectApplication(this)
    }
    @Inject
    lateinit var androidInjector : DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

}