package com.example.twittercodechallange

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.example.twittercodechallange.dagger.ViewModelFactory
import com.example.twittercodechallange.viewmodel.WeatherDataViewModel
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    companion object {
        val STATE_CURRENT_CONDITIONS = "currentConditions"
        val STATE_STD_DEV = "stdDev"
        val STATE_STATUS_MESSAGE = "statusMessage"
        val STATE_CLOUDY = "cloudy"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val model = ViewModelProvider(this, viewModelFactory)[WeatherDataViewModel::class.java]
        val getAverageButton = findViewById<Button>(R.id.getAverageButton)

        getAverageButton.setOnClickListener {
            model.loadStandardDeviation()
        }

        // Wire the listeners to the modelView
        model.getCurrentConditions().observe(this, { currentConditions ->
            findViewById<TextView>(R.id.currentInfo).text = currentConditions
        })

        model.getStdDev().observe(this, { stdDev ->
            findViewById<TextView>(R.id.standardDeviationTextView).text = stdDev
        })

        model.getStatusMessage().observe(this, { statusMessage ->
            findViewById<TextView>(R.id.statusMessageTextView).text = statusMessage
        })

        model.getIsCloudy().observe(this, { isCloudy ->
            findViewById<ImageView>(R.id.cloudIcon).visibility = if(isCloudy) View.VISIBLE else View.GONE
        })

    }

    // This callback is called only when there is a saved instance that is previously saved by using
    // onSaveInstanceState(). We restore some state in onCreate(), while we can optionally restore
    // other state here, possibly usable after onStart() has completed.
    // The savedInstanceState Bundle is same as the one used in onCreate().
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        findViewById<TextView>(R.id.currentInfo).text = savedInstanceState.getCharSequence(STATE_CURRENT_CONDITIONS)
        findViewById<TextView>(R.id.standardDeviationTextView).text = savedInstanceState.getCharSequence(STATE_STD_DEV)
        findViewById<TextView>(R.id.statusMessageTextView).text = savedInstanceState.getCharSequence(STATE_STATUS_MESSAGE)
        findViewById<TextView>(R.id.statusMessageTextView).visibility =  savedInstanceState.getInt(STATE_CLOUDY)

    }

    // invoked when the activity may be temporarily destroyed, save the instance state here
    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putCharSequence(STATE_CURRENT_CONDITIONS, findViewById<TextView>(R.id.currentInfo).text)
            putCharSequence(STATE_STD_DEV, findViewById<TextView>(R.id.standardDeviationTextView).text)
            putCharSequence(STATE_STATUS_MESSAGE, findViewById<TextView>(R.id.statusMessageTextView).text)
            putInt(STATE_CLOUDY, findViewById<TextView>(R.id.statusMessageTextView).visibility)
        }
        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState)
    }
}