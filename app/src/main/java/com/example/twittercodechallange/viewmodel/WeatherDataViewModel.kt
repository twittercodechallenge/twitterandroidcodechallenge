package com.example.twittercodechallange.viewmodel

import android.graphics.Color
import android.text.SpannableStringBuilder
import androidx.core.text.bold
import androidx.core.text.color
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.ExampleApplication
import com.example.twittercodechallange.R
import com.example.twittercodechallange.converter.ModelToViewModelConverter
import com.example.twittercodechallange.model.GetDataResponse
import com.example.twittercodechallange.repository.DataRepository
import com.example.twittercodechallange.util.DateUtil
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function5
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

/**
 * ViewModel is ideal for storing and managing UI-related data while the user is actively using the application.
 * It allows quick access to UI data and helps you avoid re-fetching data from network or disk across rotation,
 * window resizing, and other commonly occurring configuration changes.
 */
class WeatherDataViewModel
@Inject constructor(
    var dataRepository: DataRepository
) : ViewModel() {

    private val application = ExampleApplication.getInstance()
    private val currentConditions: MutableLiveData<SpannableStringBuilder> = MutableLiveData<SpannableStringBuilder>()
    private val stdDev: MutableLiveData<SpannableStringBuilder> = MutableLiveData<SpannableStringBuilder>()
    private val statusMessage: MutableLiveData<SpannableStringBuilder> = MutableLiveData<SpannableStringBuilder>()
    private val isCloudy: MutableLiveData<Boolean> = MutableLiveData<Boolean>()

    init {
        loadCurrentConditions()
    }

    fun getCurrentConditions(): LiveData<SpannableStringBuilder> {
        return currentConditions
    }

    fun getStdDev(): LiveData<SpannableStringBuilder> {
        return stdDev
    }

    fun getStatusMessage(): LiveData<SpannableStringBuilder> {
        return statusMessage;
    }

    fun getIsCloudy(): LiveData<Boolean> {
        return isCloudy;
    }

    fun getErrorStringForErrorCode(errorCode: ModelToViewModelConverter.ErrorCode): String {
        when{
            errorCode == ModelToViewModelConverter.ErrorCode.CloudInformationMissing -> return application.getString(R.string.error_cloud_information_missing)
            errorCode == ModelToViewModelConverter.ErrorCode.WeatherInformationMissing -> return application.getString(R.string.error_weather_information_missing)
            errorCode == ModelToViewModelConverter.ErrorCode.WindInformationMissing -> return application.getString(R.string.error_wind_information_missing)
            errorCode == ModelToViewModelConverter.ErrorCode.ErrorInvalidCloudPercentage -> return application.getString(R.string.error_invalid_cloud_percentage)
            errorCode == ModelToViewModelConverter.ErrorCode.Ok -> return ExampleApplication.getInstance().getString(R.string.error_none)
        }
        return "ok"
    }

    fun loadCurrentConditions() {
        dataRepository.getDataForDay(Calendar.getInstance().time)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {response ->
                    // Call the converter function to convert incoming data from the server to strings
                    val currentConditionsViewModel = ModelToViewModelConverter.convertCurrentConditions(response)
                    if (currentConditionsViewModel.conversionErrorCode == ModelToViewModelConverter.ErrorCode.Ok) {
                        displayCurrentConditions(currentConditionsViewModel)
                    } else {
                        displayErrorCondition(getErrorStringForErrorCode(currentConditionsViewModel.conversionErrorCode))
                    }
                },
                {error ->
                    displayErrorCondition(error.message ?: application.getString(R.string.error_unknown_error))
                },

            )
    }

    fun displayCurrentConditions(currentConditionsViewModel: CurrentConditions) {
        currentConditions.value = SpannableStringBuilder()
            .append(application.getString(R.string.display_current_temperature))
            .bold {  append(application.getString(R.string.display_temp_values, currentConditionsViewModel.tempC, currentConditionsViewModel.tempF)) }
            .append(application.getString(R.string.display_current_wind))
            .bold {  append("${currentConditionsViewModel.windSpeed} ") }

        statusMessage.value = SpannableStringBuilder()
            .color(Color.BLACK) {bold { append(application.getString(R.string.stat_display_ok)) }}

        isCloudy.value = currentConditionsViewModel.cloudy
    }

    fun displayErrorCondition(errorMessage: String) {
        currentConditions.value = SpannableStringBuilder()
            .color(Color.RED) {bold { append(application.getString(R.string.error_data_not_available)) }}

        statusMessage.value = SpannableStringBuilder()
            .color(Color.RED) {bold { append(application.getString(R.string.error_data_retrieval_failed)) }}
            .append(errorMessage)

        isCloudy.value = false
    }

    fun displayStdDev(stdDevViewModel: StandardDeviationData) {
        stdDev.value = SpannableStringBuilder()
            .append(application.getString(R.string.display_std_dev))
            .bold {  append("${stdDevViewModel.stdValue}") }

        statusMessage.value = SpannableStringBuilder()
            .color(Color.BLACK) {bold { append(application.getString(R.string.stat_display_ok)) }}
    }
    fun displayStdDevError(stdDevViewModel: StandardDeviationData) {
        stdDev.value = SpannableStringBuilder()
            .append(application.getString(R.string.display_std_dev))
            .bold {  append(stdDevViewModel.stdValue) }

        statusMessage.value = SpannableStringBuilder()
            .color(Color.BLACK) {bold { append(application.getString(R.string.stat_display_ok)) }}
    }

    fun loadStandardDeviation() {
        val todayCall = dataRepository.getDataForDay(DateUtil.getDateForToday()).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
        val todayPlus1Call = dataRepository.getDataForDay(DateUtil.getDateForDayFromToday(1)).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
        val todayPlus2Call = dataRepository.getDataForDay(DateUtil.getDateForDayFromToday(2)).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
        val todayPlus3Call = dataRepository.getDataForDay(DateUtil.getDateForDayFromToday(3)).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
        val todayPlus4Call = dataRepository.getDataForDay(DateUtil.getDateForDayFromToday(4)).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

        try {
            Observable.zip(todayCall, todayPlus1Call, todayPlus2Call, todayPlus3Call, todayPlus4Call,
                Function5
                { resp1: GetDataResponse, resp2: GetDataResponse, resp3: GetDataResponse, resp4: GetDataResponse, resp5: GetDataResponse ->
                    ModelToViewModelConverter.CalculateStdDevForFiveDays(resp1, resp2, resp3, resp4, resp5)
                }).subscribe(
                { stdData ->
                    if (stdData.errorCode == ModelToViewModelConverter.ErrorCode.Ok) {
                        displayStdDev(stdData)
                    } else {
                        displayStdDevError(stdData)
                    }
                },
                { error ->
                    displayStdDevError(StandardDeviationData(ModelToViewModelConverter.ErrorCode.UnknownError, error.message.toString()))
                },
            )
        } catch(e: Exception) {
            displayStdDevError(StandardDeviationData(ModelToViewModelConverter.ErrorCode.UnknownError, e.message ?: application.getString(R.string.error_unknown_error)))
        }
    }
}