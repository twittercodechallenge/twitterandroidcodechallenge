package com.example.twittercodechallange.viewmodel

import com.example.twittercodechallange.converter.ModelToViewModelConverter

data class StandardDeviationData(val errorCode: ModelToViewModelConverter.ErrorCode, val stdValue: String)
