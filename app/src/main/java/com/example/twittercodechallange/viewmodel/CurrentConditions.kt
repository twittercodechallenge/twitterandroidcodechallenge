package com.example.twittercodechallange.viewmodel

import com.example.twittercodechallange.converter.ModelToViewModelConverter

data class CurrentConditions(val conversionErrorCode: ModelToViewModelConverter.ErrorCode, val tempC: String, val tempF: String, val windSpeed: String, val cloudy: Boolean)
