package com.example.twittercodechallange.converter

import com.example.twittercodechallange.model.GetDataResponse
import com.example.twittercodechallange.util.StandardDeviationCalculator
import com.example.twittercodechallange.util.TemperatureConverter
import com.example.twittercodechallange.viewmodel.CurrentConditions
import com.example.twittercodechallange.viewmodel.StandardDeviationData
import io.reactivex.Observable

/**
 *
 */
object ModelToViewModelConverter {
    enum class ErrorCode {
        Ok,
        CloudInformationMissing,
        WeatherInformationMissing,
        WindInformationMissing,
        ErrorInvalidCloudPercentage,
        UnknownError
    }

    /**
     * Response from the server should nut be null.
     */
    private fun checkForNulls(response: GetDataResponse): ErrorCode {
        when {
            response.clouds == null -> return ErrorCode.CloudInformationMissing
            response.weather == null -> return ErrorCode.WeatherInformationMissing
            response.wind == null -> return ErrorCode.WindInformationMissing
        }

        return ErrorCode.Ok
    }

    /**
     * This is where we validate data. Whatever validation we do for the data goes in here.
     */
    private fun checkForDataRanges(response: GetDataResponse): ErrorCode {
        if (response.clouds?.cloudiness ?: -1 < 0  || response.clouds?.cloudiness ?: 101 > 100) {
            return ErrorCode.ErrorInvalidCloudPercentage
        }

        return ErrorCode.Ok
    }

    /**
     * This is the top level function for validating the integrity of the data we received.
     */
    private fun validateDataIntegrity(response: GetDataResponse): ErrorCode {
        // Check for nulls
        val nullCheckResult = checkForNulls(response)
        // If null check fails, return the error
        if(nullCheckResult != ErrorCode.Ok){
            return nullCheckResult
        }

        // Check for range
        val rangeCheckResults = checkForDataRanges(response)
        // If range check fails, return the error
        if(rangeCheckResults != ErrorCode.Ok){
            return rangeCheckResults
        }

        return ErrorCode.Ok
    }

    /**
     * We get a response from the server and this method parses out the response. It will check for data integrity and then parse.
     * Data integrity validation is done with functions above.
     */
    fun convertCurrentConditions(response: GetDataResponse) : CurrentConditions {
        var cloudy = false
        var tempC = ""
        var tempF = ""
        var windSpeed = ""

        // Validate the data integrity first. If fails then return immediately.
        val validateDataResults = validateDataIntegrity(response)
        if (validateDataResults != ErrorCode.Ok) {
            return CurrentConditions(validateDataResults, tempC, tempF, windSpeed, cloudy)
        }

        // At this point we know we have no nulls and range is ok. Start processing the data.
        if (response.clouds?.cloudiness!! > 50) {
            cloudy = true
        }

        // We have already checked for null conditions and we know we are safe at this point.
        tempC = "${response.weather?.temp}C"
        val tempInF =
            TemperatureConverter.celsiusToFahrenheit(response.weather?.temp!!.toFloat())
                .toInt()
        tempF = "${tempInF}F"

        windSpeed = "${response.wind!!.speed}MPH"

        return CurrentConditions(ErrorCode.Ok, tempC, tempF, windSpeed, cloudy)
    }

    fun validateAndExtractTempInAnArray(  resp1: GetDataResponse,
                                          resp2: GetDataResponse,
                                          resp3: GetDataResponse,
                                          resp4: GetDataResponse,
                                          resp5: GetDataResponse): Pair<ErrorCode, List<Double>?>{
        var temperatureList: List<Double>? = null
        var errorCode: ErrorCode = ErrorCode.UnknownError

        Observable
            .just(resp1, resp2, resp3, resp4, resp5)
            .map {
                    item ->

                val parsedData = convertCurrentConditions(item)
                if (parsedData.conversionErrorCode != ErrorCode.Ok) {
                    throw Exception("test")
                }
                // Since no error code in parsing, we now it is not null
                //sum += item.weather?.temp!!
                item.weather?.temp!!
            }
            .toList()
            .subscribe {
                    list ->
                        temperatureList = list
                        errorCode = ErrorCode.Ok
            }

        return Pair(ErrorCode.Ok, temperatureList)

    }
    fun CalculateStdDevForFiveDays(
        resp1: GetDataResponse,
        resp2: GetDataResponse,
        resp3: GetDataResponse,
        resp4: GetDataResponse,
        resp5: GetDataResponse
    ): StandardDeviationData {
        val validationReturn = validateAndExtractTempInAnArray(resp1, resp2, resp3, resp4, resp5)

        if (validationReturn.first == ErrorCode.Ok && validationReturn.second != null){
            val temperatureArray = validationReturn.second!!

            var stdDev: Double = StandardDeviationCalculator.calculate(temperatureArray)

            return StandardDeviationData(ErrorCode.Ok, stdDev.toString())
        }

        return StandardDeviationData(validationReturn.first, "")
    }
}