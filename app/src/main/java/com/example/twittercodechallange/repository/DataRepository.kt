package com.example.twittercodechallange.repository

import com.example.twittercodechallange.model.GetDataResponse
import java.util.*
import io.reactivex.Observable

interface DataRepository {
    fun getDataForDay(date: Date) : Observable<GetDataResponse>
}