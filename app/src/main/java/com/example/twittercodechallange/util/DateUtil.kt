package com.example.twittercodechallange.util

import java.util.*

object DateUtil {
    fun getDateForToday(): Date {
        return Calendar.getInstance().time
    }

    fun getDateForDayFromToday(dayOffset: Int): Date {
        val cal = Calendar.getInstance()
        cal.time = Date()
        cal.add(Calendar.DAY_OF_YEAR, 1)
        return cal.time
    }
}