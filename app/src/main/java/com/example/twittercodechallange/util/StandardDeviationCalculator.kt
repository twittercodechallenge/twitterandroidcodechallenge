package com.example.twittercodechallange.util

import io.reactivex.Observable
import kotlin.math.pow

object StandardDeviationCalculator {
    /**
     * Find average value
     */
    fun calculateAverage(temperatureArray: List<Double>) : Double {

        var sum = 0.0
        var average = 0.0

        Observable
            .just(temperatureArray)
            .flatMapIterable {item-> item  }
            .map {
                    temperature ->
                sum += temperature
                temperature
            }
            .toList()
            .subscribe {
                    list ->
                average = sum / list.size
            }

        return average
    }

    /**
     * Calculate the stddev
     */
    fun calculateStdDev(temperatureArray: List<Double>,
                        average: Double): Double {
        var sumOfDiffSquares = 0.0
        var stdDev = 0.0

        Observable
            .just(temperatureArray)
            .flatMapIterable { item -> item }
            .map { temperature ->
                val diff = temperature - average
                val diffSquared = diff.pow(2)
                sumOfDiffSquares += diffSquared
            }
            .toList()
            .subscribe {
                    list ->
                stdDev = Math.sqrt(sumOfDiffSquares/list.size)
            }

        return stdDev
    }

    fun calculate(temperatureArray: List<Double>) : Double{
        return calculateStdDev(temperatureArray, calculateAverage(temperatureArray))
    }
}