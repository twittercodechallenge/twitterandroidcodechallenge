package com.example.twittercodechallange.model

data class GetDataResponse(
	val rain: Rain? = null,
	val coord: Coord? = null,
	val weather: Weather? = null,
	val name: String? = null,
	val clouds: Clouds? = null,
	val wind: Wind? = null
)

data class Weather(
	val temp: Double? = null,
	val humidity: Int? = null,
	val pressure: Int? = null
)

data class Wind(
	val deg: Int? = null,
	val speed: Double? = null
)

data class Clouds(
	val cloudiness: Int? = null
)

data class Rain(
	val jsonMember3h: Int? = null
)

data class Coord(
	val lon: Double? = null,
	val lat: Double? = null
)

