package com.example.twittercodechallange.dagger.components

import com.example.ExampleApplication
import com.example.dagger.modules.RepositoryModule
import com.example.twittercodechallange.dagger.modules.ActivityModule
import com.example.twittercodechallange.dagger.modules.AppModule
import com.example.twittercodechallange.dagger.modules.ViewModelModule
import com.example.twittercodechallange.repository.DataRepository
import com.example.twittercodechallange.viewmodel.WeatherDataViewModel
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

//@Singleton
//@Component(modules = [AppModule::class, RepositoryModule::class])
//interface ApplicationComponent {
//    fun repository(): DataRepository
//    fun inject(viewModel: WeatherDataViewModel)
//}
//
//
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    RepositoryModule::class,
    ActivityModule::class,
    ViewModelModule::class
])
interface ApplicationComponent {

    fun injectApplication(app: ExampleApplication)

}