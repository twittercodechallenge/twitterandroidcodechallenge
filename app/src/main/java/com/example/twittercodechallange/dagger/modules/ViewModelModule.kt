package com.example.twittercodechallange.dagger.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.twittercodechallange.dagger.ViewModelFactory
import com.example.twittercodechallange.viewmodel.WeatherDataViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(WeatherDataViewModel::class)
    abstract fun weatherDataViewModel(viewModel: WeatherDataViewModel): ViewModel
}