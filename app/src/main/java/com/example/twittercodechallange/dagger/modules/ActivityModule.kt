package com.example.twittercodechallange.dagger.modules

import com.example.twittercodechallange.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector




@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity?
}